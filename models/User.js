const mongoose = require("mongoose");
const { Schema } = mongoose; //mongoose has a property called "Schema" (mongoose.Schema),
//instead of doing Schema = mongoose.Schema I can do this instead

const userSchema = new Schema({
  googleId: String
});

mongoose.model("users", userSchema);
